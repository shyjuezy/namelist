//
//  SampleData.swift
//  NameList
//
//  Created by SHYJU VISWAMBARAN on 2/22/16.
//  Copyright © 2016 SHYJU VISWAMBARAN. All rights reserved.
//

import Foundation

var playersData: [Player] = [
Player(name: "name1", game: "Chess", rating: 1),
Player(name: "name2", game: "Tennis", rating: 2),
Player(name: "name2", game: "Tennis", rating: 3)]

var games:[String] = [
    "Tennis",
    "Chess",
    "Poker",
    "Spin the Bottle",
    "paint Ball",
    "Tic-Tac-Toe"]