//
//  PlayerDetailsTVC.swift
//  NameList
//
//  Created by SHYJU VISWAMBARAN on 2/22/16.
//  Copyright © 2016 SHYJU VISWAMBARAN. All rights reserved.
//

import UIKit

class PlayerDetailsTVC: UITableViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var detailLabel: UILabel!
    
    var game:String? = games[0] {
        didSet {
            detailLabel.text = game
        }
    }
    
    var player:Player?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 0 {
            nameTextField.becomeFirstResponder()
        }
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "saveToPlayerView" {
            player = Player(name: nameTextField.text!, game: detailLabel.text, rating: 5)
        }
    }
    
    @IBAction func unwindWithSelectedGame(segue:UIStoryboardSegue) {
        if let gamePickerTVC = segue.sourceViewController as? GamePickerTVC {
            game = gamePickerTVC.selectedGame
        }
    }
}
