//
//  Player.swift
//  NameList
//
//  Created by SHYJU VISWAMBARAN on 2/22/16.
//  Copyright © 2016 SHYJU VISWAMBARAN. All rights reserved.
//

//import Foundation

import UIKit

struct Player {
    var name: String?
    var game: String?
    var rating: Int
    
    init(name: String?, game: String?, rating: Int) {
        self.name = name
        self.game = game
        self.rating = rating
    }
}